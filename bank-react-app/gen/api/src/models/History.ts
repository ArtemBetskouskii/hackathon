/* tslint:disable */
/* eslint-disable */
/**
 * Bank service
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface History
 */
export interface History {
    /**
     * 
     * @type {string}
     * @memberof History
     */
    product: string;
    /**
     * 
     * @type {string}
     * @memberof History
     */
    bankName: string;
    /**
     * 
     * @type {string}
     * @memberof History
     */
    price: string;
    /**
     * 
     * @type {string}
     * @memberof History
     */
    date: string;
}

export function HistoryFromJSON(json: any): History {
    return HistoryFromJSONTyped(json, false);
}

export function HistoryFromJSONTyped(json: any, ignoreDiscriminator: boolean): History {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'product': json['product'],
        'bankName': json['bankName'],
        'price': json['price'],
        'date': json['date'],
    };
}

export function HistoryToJSON(value?: History | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'product': value.product,
        'bankName': value.bankName,
        'price': value.price,
        'date': value.date,
    };
}


