import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class Banks extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            banks: []
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(banks) {
        const response = await api.banks({});
        this.setState({ banks: response });
     }


    render() {
        return <div class="filter">
            <h2>Your banks</h2>
            <div class="catalog_osnova">
                {this.state.banks.map(
                    (bank) =>
                    <div>
                       <div align="center" class="book" >
                            <a href="/banks/:id"><img src={bank.photo} width="290px" height="290px" alt="bank" /></a><br />
                            <a href="">{bank.bankName}</a><br />{bank.id}
                            <button>Show bank bills</button>
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(Banks);