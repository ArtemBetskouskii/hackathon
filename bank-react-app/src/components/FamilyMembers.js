import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import '../App.css';

const api = new Api.DefaultApi()

class FamilyMembers extends React.Component {

    constructor(props) {
        super(props);
        const id = '00001';

        this.state = {
            bank: [],
            id: id
        };

        this.handleReload = this.handleReload.bind(this);
        this.handleReload();
    }


        async handleReload(exhibit) {
            const response = await api.bank({ id: this.state.id });
            this.setState({ bank: response });
        }


    render() {
        return <div class="filter">
           <div class="catalog_osnova">
                {this.state.bank.map(
                    (bank) =>
                    <div>
                        <div align="center"class="one_book" >
                            <img src={bank.photo} width="30px" height="30px" alt="book" /><br />
                            <div class ="one_book_info__1">
                            <span class="book__name">{bank.name}</span><br />
                            <span class="book__author">Bill id:{bank.id}</span><br />
                            <div class="book_descriptions">On balance: {bank.balance}</div><br />
                            </div>
                        </div>
                    </div>)}
            </div>
        </div>
    }
}

export default withRouter(FamilyMembers);