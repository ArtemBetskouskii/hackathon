import './App.css';
import React from 'react';
import Banks from './components/Banks';
import FamilyMembers from './components/FamilyMembers';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";

function App() {
  return (

<Router>
    <div>
        <nav>
          <ul>
            <li>
              <Link to="/home">Home</Link>
            </li>
            <li>
              <Link to="/banks">Banks</Link>
            </li>
            <li>
              <Link to="/banks/:id">Bills of our family</Link>
            </li>
            <li>
              <Link to="/about">About us</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <div className="App">
         
        <section> 
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/banks">
              <FamilyMembers />
            </Route>
            <Route path="/history">
              <Banks />
            </Route>
            <Route path="/banks/:id">
              <FamilyMembers />
            </Route>
            
            <Route path="/home">
              <Home />              
            </Route>

            <Route path="*">
              <Redirect to={`/home`} />
            </Route>
          </Switch>
        </section>
      </div>
    </div>
    </Router>
  );
}

function About() {
  return (
    <div>
      <h2>
        About us
      </h2>
      <p>Here is the description of the service and necessary terms.</p>
    </div>
  );
}

function Home() {
  return (
    <div>
			<h1 class="logo_tekst">Family bank</h1>
			<div>
			<h2>About us</h2>
			<p> Online Bank, where you can check your bills in all of your banks</p>
			<a href="/banks"><button>Banks</button></a>
			<a href="/banks/:id"> <button>Family bills</button></a>
		</div>
    </div>
  );
}

export default App;